## debian/tests/makecheck.tst -- GAP Test script
## script format: GAP Reference Manual subsection 7.9.2 Test (GAP 4r8)
##
## inspired by `AvailabilityTest' in `PackageInfo.g'
gap> AdHocAvailabilityTest :=
> function()
>   local path, answer;
>   answer := true;
>   path := DirectoriesPackagePrograms( "guava" );
>   if ForAny( ["desauto", "leonconv", "wtdist"],
>       f -> Filename( path, f ) = fail
>       ) then
>     Print( "#I  Cannot find at least one of the C code programs.\n" );
>     answer := false;
>   fi;
>   return answer;
> end;;
gap> TestPackageAvailability( "guava" , "=3.19" , true );
"/usr/share/gap/pkg/guava/"
gap> AdHocAvailabilityTest();
true
gap> TestPackage( "guava" );;
#I  No errors detected while testing package guava version 3.19
#I  using the test file `/usr/share/gap/pkg/guava/tst/guava.tst'

##
## eos
